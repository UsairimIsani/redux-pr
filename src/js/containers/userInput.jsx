import React , { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { collectUserData } from "../actions/collectUserData";
import { selectUser } from "../actions/index";
 
class UserInput extends Component{
    constructor(props){
        super(props);
        this.state = {
            userFirst : "",
            userLast : "",
            userAge : Number("")
        }
        this.handleUsers = this.handleUsers.bind(this);
        this.renderList = this.renderList.bind(this);
    }
    
    handleUsers(e){
        e.preventDefault();
        
    //     const users = Array.from(this.props.newUsers);
    //     users.push({
    //         first : this.state.userFirst,
    //         last : this.state.userLast,
    //         Age : this.state.Age
    // });
    const users = { 
        first : this.state.userFirst,
        last : this.state.userLast,
        age : this.state.userAge
    }
    this.props.collectUserData(users);
    
}
renderList(){
    return (
        <div>
            <form action="POST" onSubmit={this.handleUsers}>
                <input type="text" onChange={(e) => this.setState({userFirst: e.target.value})} value={this.state.userFirst}/>
                <input type="text" onChange={(e) => this.setState({userLast:e.target.value})} value={this.state.userLast}/>
                <input type="number" onChange={(e) => this.setState({userAge :e.target.value})} value={this.state.userAge}/>
                <input type="submit" value="Submit"/>
            </form>
        </div>
    )
}
    render(){
    if(this.props.newUsers != null || this.props.newUsers != undefined){
        return(
        <div>
            {this.renderList()}
             <h1>{this.props.newUsers.users.first}</h1>
        </div>); 
    }else{
        return(
        <div>
            {this.renderList()}
             
        </div>); 
    }
 

    

}}
function mapStateToProps(state){
    return {
        users: state.users,
        newUsers : state.newUsers,
         
    }
};
function matchDispatchToProps(dispatch){
    return bindActionCreators({collectUserData : collectUserData,selectUser:selectUser}, dispatch)
};

export default connect(mapStateToProps,matchDispatchToProps)(UserInput);

