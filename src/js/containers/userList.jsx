import React , { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { selectUser } from "../actions/index"

class UserList extends Component{
    // constructor(props){
    //     super(props);
    //     this.renderList = this.renderList.bind(this);
    // }
    renderList(){
        return this.props.users.map(
            (user,idx) => {
                return (
                   
                    <li key={idx} onClick={() => this.props.selectUser(user)}>{user.first} {user.last}</li>    
                   
                );
            }
        );
    }
    render(){
        return (
            <div>
                <ul>{this.renderList()}</ul>
             
            </div>
        )
    }
}
function mapStateToProps(state){
    return {
        users : state.users,
        newUsers : state.newUsers
    }
}
function matchDispatchToProps(dispatch){
    return bindActionCreators({selectUser : selectUser}, dispatch);
}
export default connect(mapStateToProps,matchDispatchToProps)(UserList);