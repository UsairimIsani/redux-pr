import { combineReducers } from "redux";
import userRedcer from "./usersReducer";
import reducer  from "./newUserInputReducer";
const rootReducer = combineReducers({
    users : userRedcer,
    newUsers : reducer
})
export default rootReducer;