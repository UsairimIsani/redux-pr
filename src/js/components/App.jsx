import React , { Component } from "react";
import Userlist from "../containers/userList.jsx";
import UserInput from "../containers/userInput.jsx";
const App = ( ) => (
    <div>
        <h2>User Name List : </h2>
        <Userlist></Userlist>
        <h2>User Details : </h2>
        <UserInput />
    </div>
);



export default App;